#!/bin/sh
#
# gbp_retag_commit - retag a revision of a debian package, gbp style
#
# Copyright (C) 2015  Antonio Ospite <ao2@ao2.it>
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

set -e

usage() {
  echo "usage: $(basename $0) <COMMIT_ID>";
}

HASH="$1"
git cat-file -e "${HASH}^{commit}" || { usage 1>&2; exit 1; }

CHANGELOG_HEADER="$(git show $HASH:debian/changelog | head -1)"
if [ "x$CHANGELOG_HEADER" = "x" ];
then
  echo "Check that the active branch is a debian branch" 1>&2
  exit 1
fi

# TODO: give a warning or an error if this is an UNRELEASED version?

PACKAGE=$(echo "$CHANGELOG_HEADER" | cut -d ' ' -f 1)
VERSION=$(echo "$CHANGELOG_HEADER" | cut -d '(' -f 2 | cut -d ')' -f 1 )

# Use the same default values of gbp-buildpackage
TAG="debian/$VERSION"
TAG_MESSAGE="$PACKAGE Debian release $VERSION"

# TODO: maybe drop the older $TAG if it exists?

GIT_COMMITTER_DATE="$(git show --format=%aD $HASH | head -1)" \
  git tag -a "$TAG" $HASH -m "$TAG_MESSAGE"
